<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title> Power Industrial - @yield('title')</title>
    
 

    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css" >
    
    
</head>
<body class="fondo">

    <header ></header>


    <div class="container">
        @yield('content')
    </div>

</body>


<script src="/assets/bootstrap/js/bootstrap.min.js"></script>


</html>