@extends('pagebase')

@section('css')
<style>
@media (max-width: 700px) {
    .deleteButton span {
        display: none;
    }
    .deleteButton strong {
        display: inherit;
    }
}
@media (min-width: 701px) {
    .deleteButton strong {
        display: none;
    }
}

@media (max-width: 734){
    .tabla{
        overflow-x: scroll;
    }
}
</style>
@endsection

@section('contenido')
<div class="container mt-4">
    <div class="col-12 px-3 tabla">@csrf<table class="table table-striped">
        <thead class="thead-dark"><tr>
            <th scope="col">Imagen</th>
            <th scope="col">Producto</th>
            <th scope="col">Marca</th>
            <th scope="col">Precio</th>
            <th scope="col"></th>
        </tr></thead>
        <tbody>
        @foreach($datos as $item)
        <tr id="tr{{$item->id}}">
            <td>
                @if(null !== $item->image)
                <img src="/images/productos/{{$item->image}}" class="img-fluid" style='max-height: 200px'>
                @else
                <div class="fakeimg rounded" style="height: 50px"></div>
                @endif
            </td>
            <td>{{$item->Nombre}}</td>
            <td>{{$item->Marca}}</td>
            <td>&dollar;{{$item->Precio}}</td>
            <td><div class="d-flex justify-content-center">
                <div class="px-3"><button style="width: 100%" class="btn btn-danger deleteButton" onclick="quitar({{$item->id}})">
                    <span>Quitar producto</span><strong>&times;</strong></button></div>
            </div></td>
        </tr>
        @endforeach
        </tbody>
    </table></div>
</div>
@endsection

@section('Scripts')
<script>
    function quitar(id){
        $.ajax({
            url: "{{url('quitarCarrito')}}/" + id,
            type: 'DELETE',
            data: {'_token': $("input[name='_token']").val()},
            success: function(data){
                if(data == 'success'){
                    $("#tr" + id).remove();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
</script>
@endsection