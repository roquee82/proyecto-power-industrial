@extends('layouts.app')

@section('title', 'Panel de control')

@section('content')

<br><br><br><br>
<form>

<div class="container">
    <div class="row justify-content-center">
        <div class="form-group">
            <div class="col-xl-12 bg-white ">
                <br><br><br>
                <link rel="stylesheet" href="css/estilo1.css" type="text/css"> 
                <label for="" >Usuario</label>
                <input type="text" class="form-control">
                
                <br>
                <button type="submit" class="btn btn-primary" >{{ $roque }}</button>
            </div>
                
        </div>


        <div class="container">
        @yield('content')
        </div>

    </div>
</div>
</form>

   
@endsection
