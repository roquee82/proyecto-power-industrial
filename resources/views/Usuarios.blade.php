@extends('pagebase')

@section('css')
<style>
    @media (max-width: 700px) {
    .delete span {
        display: none;
    }
    .delete strong {
        display: inherit;
    }
}
@media (min-width: 701px) {
    .delete strong {
        display: none;
    }
}
</style>
@endsection

@section('contenido')
<div class="container">
    <div class="row">
        <div class="p-3">
            <form id="formUser">
                @csrf
                <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                        <label class="col-form-label">Nombre</label>
                        <input type="text" class="form-control" name="name" required maxlength="255">
                        <span class="invalid-feedback" role="alert" style="display: none" id="EName"></span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                        <label class="col-form-label">Apellidos</label>
                        <input type="text" name="apellidos" class="form-control" required maxlength="255">
                        <span class="invalid-feedback" role="alert" style="display: none" id="ELastName"></span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                        <label class="col-form-label">Correo</label>
                        <input type="email" name="email" required class="form-control" maxlength="255">
                        <span class="invalid-feedback" role="alert" style="display: none" id="EMail"></span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                        <label class="col-form-label">Tel&eacute;fono</label>
                        <input type="tel" name="phone" class="form-control" maxlength="255">
                        <span class="invalid-feedback" role="alert" style="display: none" id="EPhone"></span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                        <label class="col-form-label">Contrase&ntilde;a</label>
                        <input type="password" name="password" required class="form-control" maxlength="255">
                        <span class="invalid-feedback" role="alert" style="display: none" id="EPassword"></span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                        <label class="col-form-label">Confirmar contraseña</label>
                        <input type="password" name="password-confirm" class="form-control" maxlength="255">
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label class="col-form-label">Cargo</label>
                        <select name="tipo" class="form-control">
                            <option value="empleado">Empleado</option>
                            <option value="admin">Aministrador</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="d-flex justify-content-center align-items-center">
                        <div class="p-3">
                            <button style="width: 100%" class="btn btn-success" type="submit">Registrar empleado</button>
                        </div>
                    </div>
                </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="col-12 px-4 tabla" style="overflow: auto;">
<table class="table table-striped">
    <thead class="thead-dark"><tr>
        <th scope="col">Nombre(s)</th>
        <th scope="col">Apellidos</th>
        <th scope="col">Tel&eacute;fono</th>
        <th scope="col">Correo</th>
        <th scope="col">Cargo</th>
        <th scope="col"></th>
    </tr></thead>
    <tbody id="table">
        @foreach ($employees as $empleado)
        <tr id="id{{$empleado->id}}">
            <td>{{$empleado->name}}</td>
            <td>{{$empleado->apellidos}}</td>
            <td>{{$empleado->telefono}}</td>
            <td>{{$empleado->email}}</td>
            <td>{{$empleado->tipo}}</td>
            <td><button class="btn btn-danger delete" onclick="revocar({{$empleado->id}})"><span>Revocar cargo</span>
            <strong>&times;</strong></button></td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection

@section('Scripts')
<script>
    $("#formUser").on('submit', function(evt){
        evt.preventDefault(evt);
        $("#formUser").find('input').removeClass('is-invalid');
        $("span.invalid-feedback").css('display', 'none');
        $("span.invalid-feedback").html("");
        var patt = /[a-zA-Z]/g;
        var flag = true;
        if($("#formUser").find('input[name="name"]').val().length < 1){
            $("#EName").html("El campo no debe estar vacío");
            $("#Name").css("display", 'block');
            $("#formUser").find('input[name="name"]').addClass('is-invalid');
            flag = false;
        }
        if($("#formUser").find('input[name="apellidos"]').val().length < 1){
            $("#ELastName").html("El campo no debe estar vacío");
            $("#ELastName").css("display", 'block');
            $("#formUser").find('input[name="apellidos"]').addClass('is-invalid');
            flag = false;
        }
        if($("#formUser").find('input[name="email"]').val().length < 1){
            $("#EMail").html("El campo no debe estar vacío");
            $("#EMail").css("display", 'block');
            $("#formUser").find('input[name="email"]').addClass('is-invalid');
            flag = false;
        }
        if(patt.test($("#formUser").find('input[name="phone"]').val())){
            $("#EPhone").html("El campo no debe contener letras");
            $("#EPhone").css("display", 'block');
            $("#formUser").find('input[name="phone"]').addClass('is-invalid');
            flag = false;
        }
        if($("#formUser").find('input[name="password"]').val().length < 8){
            $("#EPassword").html("El campo debe tener al menos 8 carácteres");
            $("#EPassword").css("display", 'block');
            $("#formUser").find('input[name="password"]').addClass('is-invalid');
            flag = false;
        }
        if($("#formUser").find('input[name="password-confirm"]').val().length < 1){
            $("#EPassword").html("Debe escribir la confirmación de la contraseña");
            $("#EPassword").css("display", 'block');
            $("#formUser").find('input[name="password-confirm"]').addClass('is-invalid');
            flag = false;
        }
        if($("#formUser").find('input[name="password"]').val() != $("#formUser").find('input[name="password-confirm"]').val()){
            $("#EPassword").html("Las contraseñas no coinciden");
            $("#EPassword").css("display", 'block');
            $("#formUser").find('input[name="password"]').addClass('is-invalid');
            flag = false;
        }
        if(flag){
            $.ajax({
                url: "{{route('empleado.agregar')}}",
                type: 'POST',
                data: $("#formUser").serialize(),
                success: function(data){
                    if(data != 'failure'){
                        $("#table").after('<tr id="id'+ data.id+'"><td>'+data.name+"</td><td>"+data.apellidos+"</td><td>"+
                            data.telefono+"</td><td>"+data.email+"</td><td>"+data.tipo+
                            '<td><button class="btn btn-danger delete" onclick="revocar('+data.id+
                            ')"><span>Revocar cargo</span><strong>&times;</strong></button></td></tr>'
                        );
                        $("#formUser").find('input').removeClass('is-invalid');
                        $("span.invalid-feedback").css('display', 'none');
                        $("span.invalid-feedback").html("");
                    }
                },
                error: function(error){
                    var message = error.responseJSON.message;
                    if(message.startsWith("SQLSTATE[23000]")){
                        $("#EMail").html("El correo ya ha sido utilizado");
                        $("#EMail").css("display", 'block');
                        $("#formUser").find('input[name="email"]').addClass('is-invalid');
                    }else{
                        console.log(error);
                    }
                    
                }
            });
        }
    });

    function revocar(id){
        var datos = {"_token": $("input[name='_token']").val()};
        $.ajax({
            url: "{{url('bajaEmpleado')}}/" + id,
            data: datos,
            type: 'DELETE',
            success: function(data){
                if(data != 'failure'){
                    $("#id" + id).remove();
                }
            },
            error: function(error){
                console.log(error);
            }
        });
    }
</script>
@endsection