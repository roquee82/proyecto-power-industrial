@extends('pagebase')

@section('css')
  <style>
    .wrapper {
        display: flex;
        width: 100%;
        align-items: stretch;
    }

#sidebar {
    min-width: 250px;
    max-width: 250px;
    /*background: #7386D5;*/
    color: black;
    transition: all 0.3s;
}

#sidebar.active {
    margin-left: -250px;
}

#sidebar .sidebar-header {
    padding: 20px;
    color: black;
    /*background: #6d7fcc;*/
}

/* ---------------------------------------------------
    CONTENT STYLE
----------------------------------------------------- */

#content {
    width: 100%;
    padding: 20px;
    transition: all 0.3s;
}

/* ---------------------------------------------------
    MEDIAQUERIES
----------------------------------------------------- */

@media (max-width: 768px) {
    #sidebar {
        margin-left: -250px;
    }
    #sidebar.active {
        margin-left: 0;
    }
    /*#sidebarCollapse span {
        display: none;
    }*/
}
  </style>
@endsection

@section('contenido')
<div class="container-fluid">
  <div class="row">
    <div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar" class="rounded border active">
        <div class="sidebar-header">
            <h3>Cat&aacute;logo</h3>
        </div>
        <span style="font-weight: 500" class="pl-1">Filtrar por:</span>
        <div class="input-group p-1">
            <select id="filtro" class="form-control">
              <option value="Fecha_publicacion DESC">M&aacute;s reciente</option>
              <option value="Nombre ASC">Nombre</option>
              <option value="Precio ASC">Precio m&aacute;s bajo</option>
            </select>
            <button class="btn border" id="searchBtn"><i class="fas fa-search"></i></button>
        </div>
        <div class="col-12">
          <h5>Categor&iacute;a</h5>
          <a href="{{url('CatalogoLike=PLC')}}">PLC</a><br>
          <!--div class="dropdown-divider"></div-->
          <a href="{{url('CatalogoLike=HMI')}}">HMI</a><br>
          <a href="{{url('CatalogoLike=variador')}}">Variadores</a><br>
          <a href="{{url('CatalogoLike=fuente')}}">Fuentes</a><br>
        </div>
        <br>
    </nav>
    <!--Contenido-->
      <div class="pt-0" id="content">
          <div class="p-1"><button id="sidebarCollapse" class="btn btn-info">
            <span><i class="fas fa-filter"></i> Filtros</span>
          </button></div>
          <div class="row" id='catalogo'>
          @foreach($datos as $item)
            <div class="col-md-4 col-sm-6 rounded border" data-pid="{{$item->id}}">
              @auth
                  @if(auth()->user()->tipo == 'admin')
                      <div class="d-flex justify-content-between mt-2">
                        <div><button class="btn btn-primary" data-toggle="tooltip"
                          data-placement="right" title="Editar" onclick="editar({{$item->id}})"><i class="fas fa-pencil-alt"></i></button></div>
                        <div><button class="btn btn-danger" data-id="{{$item->id}}" data-toggle="tooltip" onclick="eliminar(this)"
                          data-placement="left" title="Eliminar" data-title="{{$item->Nombre}}"><b>&times;</b></button></div>
                      </div>
                  @endif
                  @if(auth()->user()->tipo == 'cliente' && !$item->carrito)
                    <div class="d-flex justify-content-end mt-2">
                      <div><button class="btn btn-primary" data-toggle="tooltip" onclick="agregarCarrito({{$item->id}}, this)"
                        data-placement="left" title="Agregar al carrito"><i class="fas fa-cart-plus"></i></button></div>
                    </div>
                  @endif
                  @if(auth()->user()->tipo == 'cliente' && $item->carrito)
                    <div class="d-flex justify-content-end mt-2">
                      <div><button class="btn btn-primary" data-toggle="tooltip"
                        data-placement="left" title="Agregar al carrito"><i class="fas fa-check"></i></button></div>
                    </div>
                  @endif
              @endauth
              
              @if (null !== $item->image)
                <img src="/images/productos/{{$item->image}}" class="img-fluid rounded px-3 my-2" alt="Img: {{$item->Nombre}}">
              @else
                <div class="fakeimg px-3 my-2 rounded"></div>
              @endif
              <p><h5>{{$item->Nombre}}</h5>
              Marca: {{$item->Marca}}<br>
              Precio: &dollar;{{$item->Precio}}</p>
              <div class="d-flex justify-content-center mx-auto">
              <button class="btn btn-light border mb-1" onclick="verProducto({{$item->id}})">
                M&aacute;s informaci&oacute;n</button>
              </div>
            </div>
          @endforeach
          </div>
      </div>
    </div>
  </div>
</div> 

<!--Modal de confirmación al eliminar-->
<div class="modal" role="dialog" id="confirmDelete">
  <div class="modal-dialog modal-dialog-centered"><div class="modal-content">
    <div class="modal-header">
      <h3>&iquest;Desea eliminar este producto?</h3>
      <button type="button" class="close" data-dismiss="modal">
        &times;</button>
    </div>
    <div class="modal-body">
      <h5 id="deleteTitle">Producto seleccionado: </h5>
      <form id="deleteForm">
        @csrf
        <input type="number" id="deleteID" name="id" hidden>
      <div class="d-flex justify-content-between" id="deleteModalBtns">
        <div><button class="btn btn-danger" data-dismiss="modal" type="button">No</button></div>
        <div><button class="btn btn-success" type="submit" id="deleteButton">S&iacute;</button></div>
      </div>
      </form>
    </div>
  </div></div>
</div>
@endsection

@section('Scripts')
<script type="text/javascript">
  
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });

            $('#searchBtn').on('click', function(){
              var filtro = $("#filtro").val();
              $.get('CatalogoSort=' + filtro, function(data){
                var content = ""
                var permission = data.permission;
                $.each(data.products, function(index, value){
                  content = content + '<div class="col-md-4 col-sm-6 rounded border" data-pid="' + value.id + '">';
                  if(permission == "admin"){
                    content = content + '<div class="d-flex justify-content-between mt-2">'+
                        '<div><button class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Editar" onclick="editar('+
                        value.id + '"><i class="fas fa-pencil-alt"></i></button></div>'+
                        '<div><button class="btn btn-danger" data-id="'+value.id+'" data-toggle="tooltip" data-title="'+ value.Nombre+
                        '" data-placement="left" title="Eliminar" onclick="eliminar(this)"><b>&times;</b></button></div></div>';
                  }
                  if(permission == 'client'){
                    if(value.carrito == false){
                      content = content + '<div class="d-flex justify-content-end mt-2">'+
                        '<div><button class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Agregar al carrito"'+
                        ' onclick="agregarCarrito('+value.id+', this)"><i class="fas fa-cart-plus"></i></button></div></div>';
                    }else{
                      content = content + '<div class="d-flex justify-content-end mt-2">'+
                        '<div><button class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Agregar al carrito"><i class="fas fa-check"></i></button></div></div>';
                    }
                  }
                  if(value.image !== null){
                    content = content + '<img src="/images/productos/'+value.image+'" class="img-fluid rounded px-3 my-2" alt="Img: '+
                              value.Nombre+'">';
                  }else{
                    content = content + '<div class="fakeimg px-3 my-2 rounded"></div>';
                  }
                  content = content + '<p><h5>'+value.Nombre+'</h5>'+
                                      'Marca: '+value.Marca+'<br>'+
                                      'Precio: &dollar;'+value.Precio+'</p>'+
                                      '<div class="d-flex justify-content-center mx-auto">'+
                                      '<button class="btn btn-light border mb-1" onclick="verProducto('+value.id+')">'+
                                      'M&aacute;s informaci&oacute;n</button></div></div>';
                });
                $('#catalogo').html(content);
              })
            });
        });
    
    function verProducto(id){
      window.location.href = "{{url('Producto')}}/" + id;
    }

    function eliminar(element){
      $("#confirmDelete").modal('show');
      $("#deleteID").val($(element).attr('data-id'));
      $("#deleteTitle").html("Producto seleccionado: " + $(element).attr("data-title"));
    }
</script>
@auth
@if (auth()->user()->tipo == "admin")
<script>
    $("#deleteForm").on("submit", function(e){
      e.preventDefault(e);
      var id = $("#deleteID").val();
      var originalContent = $("#deleteTitle").html();
      $.ajax({
        url: "{{url('Producto')}}/" + id,
        type: "DELETE",
        data: $("#deleteForm").serialize(),
        beforeSend: function(){
          $("#deleteTitle").html("<h5>Eliminando...</h5>");
          $("#deleteModalBtns").children().css("display", "none");
        },
        success: function(data){
          var resp = JSON.parse(data);
          if(resp.respuesta == "success"){
            $("#deleteTitle").html(originalContent);
            $("#deleteModalBtns").children().css("display", "flex");
            $("div[data-pid='"+ id +"'").remove();
            $("#confirmDelete").modal("hide");
          }
          else{
            alert(data);
          }
        },
        error: function(error){
          $("#deleteTitle").html(originalContent);
          $("#deleteModalBtns").children().css("display", "flex");
          console.log(error);
        }
      });
    });

function editar(id){
  window.location.href = "{{url("editarProducto/")}}/" + id;
}
</script>
@else
<script>
  function agregarCarrito(prod, element){
  var datos = {"prod_id": prod, "_token": $("input[name='_token']").val()}
  $.ajax({
    url: '{{url("agregarCarrito")}}',
    type: 'POST',
    data: datos,
    success: function(data){
      $(element).html('<i class="fas fa-check"></i>');
      $(element).attr('onclick', null);
    },
    error: function(error){
      console.log(error);
    }
  });
}
</script>
@endif
@endauth
@endsection