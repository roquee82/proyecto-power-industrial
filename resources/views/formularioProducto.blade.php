@extends('pagebase')

@section('contenido')
<div class="container" style="margin-top: 2rem;">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <h3>Error:</h3>
            @foreach ($errors->all() as $error)
                <ul>
                    <li>{{$error}}</li>
                </ul>
            @endforeach
        </div>
    @endif
    <?/*@if (null !== Session::get('success'))
        <div class="alert alert-success alert-block">
            <button class="close" data-dismiss="alert">&times;</button>
            <strong>Carga Subida</strong>
            <img class="img-fluid" src="images/productos/{{Session::get('path')}}">
        </div>
    @endif*/?>
    <div class="row justify-content-center"><div class="col-md-10 col-sm-10">
        <form enctype="multipart/form-data" method="POST" action="{{url('/AltaProducto')}}" id="myForm">
            @csrf
        <div class="form-group row">
            <label for="nombre" class="col-md-4 col-form-label text-md-right">Nombre del producto:</label>
        
        <div class="col-sm-6">
            <input type="text" class="form-control" maxlength="255" id="nombre" required name="prod_name">
        </div></div>
        <div class="form-group row">
            <label for="precio" class="col-md-4 col-form-label text-md-right">Precio:</label>
        
        <div class="col-sm-6">
            <input type="number" class="form-control" id="precio" max="99999999.99" min="0.10" step="0.01" value="0.50" required name="price">
        </div></div>
        <div class="form-group row">
            <label for="marca" class="col-md-4 col-form-label text-md-right">Marca:</label>
        
        <div class="col-sm-6">
            <div class="form-check">
                <input type="checkbox" class="form-check-input" checked name="unknown_brand" onclick="toggleBrand(this)">
                <label class="form-check-label">Marca desconocida</label>
            </div>
            <input type="text" class="form-control" maxlength="255" id="marca" name="brand" readonly>
        </div></div>
        <div class="form-group row">
            <label for="image" class="col-md-4 col-form-label text-md-right">Imagen:</label>
        
        <div class="col-sm-6">
            <div class="mb-3"><img id="preview" src="" alt="Vista previa" class="img-fluid rounded"></div>
            <input type="file" id="image" accept="image/png, image/jpeg, image/jpg" name="image" class="form-control-file">
            <div class="mt-3"><button type="button" class="btn btn-light border" onclick="limpiarImg()">Quitar imagen</button></div>
        </div></div>
        <div class="form-group row">
            <label for="desc" class="col-md-4 col-form-label text-md-right">Descripci&oacute;n:</label>
        
        <div class="col-sm-6">
            <textarea class="form-control" id="desc" required rows="5" placeholder="Agregar descripci&oacute;n..." class="col-12" required name="desc"></textarea>
        </div></div>
        <div class="form-group row">
            <label for="link" class="col-md-4 col-form-label text-md-right">Link de Mercado Libre:</label>
        
        <div class="col-sm-6">
            <input type="text" class="form-control" maxlength="255" id="link" name="link">
            <p style="color: gray;"><i>(Opcional) Copiar y pegar link de mercado libre.</i><p>
        </div></div>
        <div class="form-group row">
            <label for="status" class="col-md-4 col-form-label text-md-right">Estado:</label>
        
        <div class="col-sm-6">
            <select name="status" id="status" class="form-control">
                <option value="activo">Normal</option>
                <option value="oferta">En oferta</option>
            </select>
        </div></div>
        <input type="number" hidden value="{{auth()->user()->id}}" name="user_id">
        </form>
        <div class="form-group d-flex justify-content-center">
            <div><button class="btn btn-primary" data-toggle="modal" data-target="#confirm">Registrar</button></div>
        </div>
    </div></div>
</div>
<div class="modal fade" id="confirm" data-backdrop="static"><div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h3>&iquest;Est&aacute; seguro de querer registrar el producto?</h3>
        </div>
        <div class="modal-body">
            <div class="d-flex justify-content-between">
                <div><button class="btn btn-danger" data-dismiss="modal">No</button></div>
                <div><button class="btn btn-success" data-dismiss="modal" onclick="subir()">S&iacute;</button></div>
            </div>
        </div>
    </div>
</div></div>
@endsection

@section('Scripts')
 <script>
     function toggleBrand(component){
         if(!component.checked){
            $('#marca').prop('required', true);
            $('#marca').prop('readonly', false);
         }
         else{
            $('#marca').prop('required', false);
            $('#marca').prop('readonly', true);
         }
     }

     function subir(){
        $("#nombre").removeClass("is-invalid");
        $("#precio").removeClass("is-invalid");
        $("#marca").removeClass("is-invalid");
        $("#desc").parent().find("span").remove();
        var flag = true;
         if(!document.getElementById("nombre").checkValidity()){
            $("#nombre").addClass("is-invalid");
            flag = false;
         }
         if(!document.getElementById("precio").checkValidity()){
            $("#precio").addClass("is-invalid");
            flag = false;
         }
         if(!$('input[type=checkbox]').is(":checked") && !document.getElementById("marca").checkValidity()){
            $("#marca").addClass("is-invalid");
            flag = false;
         }
         if(!document.getElementById("desc").checkValidity()){
            $("#desc").after('<span class="invalid-feedback" role="alert" style="display: inherit;"><strong>Completa este campo</strong></span>');
            flag = false;
         }
         if(flag){
            $("#myForm").submit();
         }
         
     }
</script>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

$("#image").change(function(){
    readURL(this);
});

function limpiarImg(){
    $('#image').val("");
    $("#preview").attr('src', '');
}
</script>
@endsection