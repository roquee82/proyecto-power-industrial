@extends('pagebase')

@section('css')
<style>
  .carousel-item img {
      width: 100%;
      height: 100%;
  }
  .blue-gradient{
    background: rgba(0,74,131,1);
    background: -moz-linear-gradient(left, rgba(0,74,131,1) 0%, rgba(0,74,131,1) 37%, rgba(7,126,217,1) 100%);
    background: -webkit-gradient(left top, right top, color-stop(0%, rgba(0,74,131,1)), color-stop(37%, rgba(0,74,131,1)), color-stop(100%, rgba(7,126,217,1)));
    background: -webkit-linear-gradient(left, rgba(0,74,131,1) 0%, rgba(0,74,131,1) 37%, rgba(7,126,217,1) 100%);
    background: -o-linear-gradient(left, rgba(0,74,131,1) 0%, rgba(0,74,131,1) 37%, rgba(7,126,217,1) 100%);
    background: -ms-linear-gradient(left, rgba(0,74,131,1) 0%, rgba(0,74,131,1) 37%, rgba(7,126,217,1) 100%);
    background: linear-gradient(to right, rgba(0,74,131,1) 0%, rgba(0,74,131,1) 37%, rgba(7,126,217,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#004a83', endColorstr='#077ed9', GradientType=1 );
  }
  .dark-control{
    text-shadow: 2px 0 0 rgb(77, 77, 77, 0.5), -2px 0 0 rgb(77, 77, 77, 0.5), 0 2px 0 rgb(77, 77, 77, 0.5), 0 -2px 0 rgb(77, 77, 77, 0.5), 1px 1px rgb(77, 77, 77, 0.5),
                  -1px -1px 0 rgb(77, 77, 77, 0.5), 1px -1px rgb(77, 77, 77, 0.5), -1px 1px 0 rgb(77, 77, 77, 0.5);
    font-size: 50px;
  }
</style>
@endsection

@section('contenido')
<div style="height: 40px;"></div>

<div class="d-flex justify-content-center"><div class="col-md-9 col-sm-10 col-11">
<div id="promo" class="carousel slide mx-4" data-ride="carousel" data-interval="4000">

  <!-- Indicators -->
  <ul class="carousel-indicators" style="background-color: rgb(75,75,75,0.5);border-radius: 4pc;">
    <li data-target="#promo" data-slide-to="0" class="active"></li>
    <li data-target="#promo" data-slide-to="1"></li>
    <li data-target="#promo" data-slide-to="2"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="/images/banner/1.jpg" alt="Todo para tu automatizaci&oacute;n">
    </div>
    <div class="carousel-item">
        <img src="/images/banner/2.jpg" alt="Tableros">
    </div>
    <div class="carousel-item">
      <img src="/images/banner/3.jpg" alt="Power Industrial">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#promo" data-slide="prev">
    <i class="fas fa-chevron-left dark-control"></i>
  </a>
  <a class="carousel-control-next" href="#promo" data-slide="next">
    <i class="fas fa-chevron-right dark-control"></i>
  </a>

</div>
</div></div>
<br />
<div class="container-fluid px-4">
  <div class="row">
    <div class="col-12 blue-gradient" style="border: solid; border-radius: 4px;">
      <div style="color: white"><h2>¿Qui&eacute;nes somos?</h2>
      <p>Somos una empresa con varios años de experiencia y 100% Jalisciense que se dedica a la compra-venta de componentes 
        con un catálogo amplio de refacciones para proyectos de Automatización, Mantenimiento y Reparación.</p></div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-12 blue-gradient" style="border: solid; border-radius: 4px;">
      <h2 style="color: white">Contacto</h2>
      <div class="row">
      <div class="col-sm-6" style="color: white">
        <p><h5><i class="far fa-envelope" style="font-size: 1.5em;"></i> renato_muro@yahoo.com.mx</h5>
          <h5><i class="fab fa-whatsapp" style="color: lime; font-size: 1.5em;"></i> Cel. 3311 07 86 69</h5>
          <h5><i class="far fa-envelope" style="font-size: 1.5em;"></i> power.industrial1@gmail.com</h5></p>
      </div>
      <div class="col-sm-6" style="color: white"><p>
        <h5><i class="fab fa-facebook-square border-icon" style="color: rgb(51, 51, 255); font-size: 1.5em;"></i>
          <a href="https://www.facebook.com/Power-industrial-1916827805209274/" style="color: #007bff;">Power Industrial</a>
        </h5>
        <h5><img src="/images/ML icon2.png" style="width: 2em; height; auto;"> Perfil en 
          <a href="https://www.mercadolibre.com.mx/perfil/POWER+INDUSTRIAL" style="color: #007bff;">Mercado Libre</a>
        </h5></p>
      </div>
      </div>
    </div>
  </div>
  <br>
  <div class="row" style="color:white;">
    <div class="col-md-5 col-sm-6">
      <div id="mapa">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3734.2385865870237!2d-103.3618772850742!3d20.619129686221747!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428ad8013f885cb%3A0xaf6a30491f7071ae!2sPower%20Industrial!5e0!3m2!1ses-419!2smx!4v1582421142433!5m2!1ses-419!2smx"
        width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
      </div>
    </div>
    <div class="col-md-7 col-sm-6">
      <div class="row blue-gradient" style="border-width: 5px; border: 2px solid black; border-radius:4px;">
      <div class="col-md-6">
        <div class="d-flex justify-content-center align-items-center pb-4">
          <div style="line-height: 37px; padding: 1rem;">
            <h2>Direcci&oacute;n</h2>
            <p><b>
              Luis Covarrubias &num; 819<br>
              Col. Primero de Mayo.<br>
              Guadalajara. Jal.<br>
            </b></p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="d-flex justify-content-center align-items-center pb-4">
          <div><p><h2 style="text-align: center">Horarios:</h2>
          Lunes a viernes de 10 A.M. a 3 P.M.<br>
          S&aacute;bados de 10 A.M. a 1 P.M.</p></div>
        </div>
      </div>
      </div>
      <div class="pt-3">
      <img src="/images/imgLocal.jpg" alt="Imagen del local" style="border-width: 5px; border: 2px solid black; border-radius:4px; width: 100%; height: auto;"></div>
    </div>
  </div>
</div>
<br>
<div class="container-fluid px-4">
  <div style="height: 3px; width: 100%; background-color: black; border-radius: 2px;"></div>
  <div class="my-3 ml-4"><h2>Promociones</h2></div>
  <br>
  <div class="row" style="border-radius: 4px; border: solid;">
    @foreach($productos as $prod)
    <div class="col-md-2 col-sm-4 col-6 mt-3">
      @if($prod->image != null)
      <img class="img-fluid" src="images/productos/{{$prod->image}}">
      @else
      <div class="fakeimg rounded"></div>
      @endif
      <p><h3 style="text-overflow: clip">{{$prod->Nombre}}</h3>
        Marca: {{$prod->Marca}}<br>
        Precio: {{$prod->Precio}}
        <div class="d-flex justify-content-center mx-auto">
          <a class="btn btn-light border mb-1" href="{{url('Producto').'/'.$prod->id}}">
            M&aacute;s informaci&oacute;n</a>
        </div>
      </p>
    </div>
    @endforeach
  </div>
</div>
<br><br>
@endsection