<!--Página sin el footer y más simple-->
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Power Industrial</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css" >
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://kit.fontawesome.com/95e544bed9.js" crossorigin="anonymous"></script>
  <style>
  .fakeimg {
    height: 200px;
    background: #aaa;
  }
  .custom-dropdown{
      display:block;
      width:100%;
      padding:.25rem 1.5rem;
      clear:both;
      font-weight:400;
      color:#212529;
      text-align:inherit;
      white-space:nowrap;
      background-color:transparent;
      border:0}
      .custom-dropdown:focus,.custom-dropdown:hover
      {color:#16181b;
      text-decoration:none;
      background-color:rgb(242, 242, 242, 0.5)}

    .border-text{
      text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff,
                  -1px -1px 0 #fff, 1px -1px #fff, -1px 1px 0 #fff;
    }
  </style>
  @yield('css')
</head>
<body>
    <div class="pt-4 py-4" style="margin-bottom:0; background-image: url('/images/img.jpg')">
        <div class="d-sm-flex justify-content-between">
          <div class="ml-2">
            <h1 style="font-family: Impact;" class="border-text">Power Industrial</h1>
          </div>
        </div>
    </div>
    @yield('content')
</body>
<footer>
@yield('footer')
</footer>
@yield('scripts')