@extends('pagebase')

@section('css')
  <style>
      .more{
          display: none;
      }
  </style>
@endsection

@section('contenido')
<div style="height: 40px;"></div>
<div class="cointainer mx-2">
    <div class="row">
        @auth @if (auth()->user()->tipo == "admin")
            <div class="col-12">
                <div class="d-flex justify-content-end mb-4">
                    <div class="mr-3"><button type="button" class="btn btn-primary" onclick='window.location.href = "{{url('editarProducto') . '/' . $info[0]->id}}";'>
                        Editar <i class="fas fa-pencil-alt"></i></button></div>
                    <div class="ml-3"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete">Eliminar <strong>&times;</strong></button></div>
                </div>
            </div>
        @endif @endauth
        <div class="col-md-6">
            @if (null !== $info[0]->image)
                <img src="/images/productos/{{$info[0]->image}}" class="img-fluid rounded" alt="Imagen: {{$info[0]->Nombre}}">
            @else
                <div class="fakeimg rounded"></div>
            @endif
        </div>
        <div class="col-md-6">
            <h2>{{$info[0]->Nombre}}</h2><br>
            <h2>&dollar;{{$info[0]->Precio}}</h3><br>
            <h4>Stock disponible: <i class="fas fa-check" style="color: green"></i></h4>
            <h4>Marca: {{$info[0]->Marca}}</h4>
            <span style="color: dimgrey">Fecha de publicacion: {{$info[0]->Fecha_publicacion}}</span>
            @if (null !== $info[0]->linkMercado)
            <div class="m-4"><a class="btn btn-warning" style="width: 100%" href="{{$info[0]->linkMercado}}">
                <img src="/images/ML icon2.png" style="height:30px; width:auto;"><strong>Link a Mercado Libre</strong></a></div>
            @endif
        </div>
        <div style="height: 3px; width: 90%; background-color: black; border-radius: 2px;" class="my-2 mx-auto"></div>
        <div class="col-12">
            <h3>Descripci&oacute;n</h3><br>
            <p>{{$info[0]->Descripcion}}</p>
        </div>
        <div style="height: 3px; width: 90%; background-color: black; border-radius: 2px;" class="my-2 mx-auto"></div>
        <div class="col-12">
            <label for="text">Pregunta al vendedor:</label>
            <form action="{{route('commit')}}" method="POST">
                @csrf
                <input type="number" hidden name="prod_id" value="{{$info[0]->id}}">
            <div class="form-inline row" id="text">
                <div class="col-md-9">
                    <input class="form-control mb-2" name="username" type="text" placeholder="Nombre de usuario" required style="width: 100%">
                    <textarea class="form-control" name="description" rows="3" style="width: 100%" placeholder="Escribe un comentario..." required></textarea>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-light border" type="submit">Enviar</button>
                </div>
            </div>
            </form>
            @if (session('confirm'))
                <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                &iexcl;{{session('confirm')}}&excl;
              </div>
            @endif
            <div class="pt-4">
                @php
                    $Primero = true;
                    $respuesta = null;
                @endphp
                <ul class="list-unstyled">
                @foreach($preguntas as $pregunta)
                    @if ($Primero)
                        <li class="media">
                            <div class="media-body rounded border mb-1 pt-2 px-2">
                                <h5>De: {{$pregunta->username}}</h5>
                                <p>{{$pregunta->Contenido}}<br>
                                <span style="color: gray;">{{$pregunta->created_at}}</span></p>
                            @php if (null !== $pregunta->respuesta){
                                $respuesta = $pregunta->respuesta;
                                echo '<div class="d-flex justify-content-end">'.
                                    '<div class="col-md-6 col-sm-9 col-12 mb-2">'.
                                        '<div class="media-body"><h5>'.$respuesta->username.'</h5>'.
                                        '<p>'.$respuesta->Contenido.'</p><span style="color: gray">'.
                                        $respuesta->created_at.'</span></div></div></div>';
                                }
                            @endphp
                            @auth @if (auth()->user()->tipo == 'admin' && null === $pregunta->respuesta)
                                <div class="d-flex justify-content-end" data-id="{{$pregunta->id}}">
                                    <div><button class="btn btn-light mb-2" onclick="responder(this, {{$pregunta->id}})"><i class="far fa-comment-alt"></i> Responder</button></div>
                                </div>
                            @endif @endauth
                            </div>
                        </li>
                        @php
                            $Primero = false;
                        @endphp
                    @else
                        <li class="media more">
                            <div class="media-body rounded border mb-1 pt-2 px-2">
                                <h5>De: {{$pregunta->username}}</h5>
                                <p>{{$pregunta->Contenido}}<br>
                                <span style="color: gray;">{{$pregunta->created_at}}</span></p>
                            @php if (null !== $pregunta->respuesta){
                                $respuesta = $pregunta->respuesta;
                                echo '<div class="d-flex justify-content-end">'.
                                    '<div class="col-md-6 col-sm-9 col-12 mb-2">'.
                                        '<div class="media-body"><h5>'.$respuesta->username.'</h5>'.
                                        '<p>'.$respuesta->Contenido.'</p><span style="color: gray">'.
                                        $respuesta->created_at.'</span></div></div></div>';
                                }
                            @endphp
                            @auth @if (auth()->user()->tipo == 'admin' && null === $pregunta->respuesta)
                                <div class="d-flex justify-content-end" data-id="{{$pregunta->id}}">
                                    <div><button class="btn btn-light mb-2" onclick="responder(this, {{$pregunta->id}})"><i class="far fa-comment-alt"></i> Responder</button></div>
                                </div>
                            @endif @endauth
                            </div>
                        </li>
                    @endif
                @endforeach
                </ul>
                @if(count($preguntas) > 1)
                    <button class="btn btn-dark" id="myBtn" onclick="verMas()">Ver m&aacute;s <i class="fas fa-angle-down"></i></button>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="mb-2"></div>
<div class="modal" role="dialog" id="confirmDelete">
    <div class="modal-dialog modal-dialog-centered"><div class="modal-content">
      <div class="modal-header">
        <h3>&iquest;Desea eliminar este producto?</h3>
        <button type="button" class="close" data-dismiss="modal">
          &times;</button>
      </div>
      <div class="modal-body">
        <div class="d-flex justify-content-between">
          <div><button class="btn btn-danger" data-dismiss="modal" type="button">No</button></div>
          <div><button class="btn btn-success" type="button" onclick="eliminar({{$info[0]->id}})">S&iacute;</button></div>
        </div>
      </div>
    </div></div>
</div>
@endsection

@section('Scripts')
<script>
    function verMas() {
    var btnText = document.getElementById("myBtn");

    if ($('.more').css('display') === "inline") {
        $('.more').css('display', 'none');
        btnText.innerHTML = "Ver m&aacute;s <i class='fas fa-angle-down'></i>";
    } else {
        $('.more').css('display', 'inline');
        btnText.innerHTML = "Ver menos <i class='fas fa-angle-up'></i>";
    }
    }
</script>
@auth @if(auth()->user()->tipo == 'admin')
<script>
    function responder(component, id){
        $(component).parent().parent().html('<div class="col-md-6 col-sm-9 col-12 mb-2"><div class="form-inline"><div class="d-flex flex-fill pr-2">'+
                                    '<textarea class="form-control" rows="2" placeholder="Escribe aqu&iacute; tu respuesta" required style="width: 100%" id="content'+id+'"></textarea>'+
                                    '</div><div class="form-group"><button class="btn btn-light border" onclick="send('+id+')">Enviar</button>'+
                                    '<button class="btn btn-danger ml-2" onclick="cancel('+id+
                                    ')"><strong>&times;</strong></button></div>'+
                                    '<input hidden type="number" value="'+id+'" name="id_pregunta"></div></div>');
    }

    function cancel(id){
        $("div[data-id="+id+"]").html('<div><button class="btn btn-light mb-2" onclick="responder(this, '+id+')"><i class="far fa-comment-alt"></i> Responder</button></div>')
    }

    function send(id){
        var data = new FormData();
        $("#content"+id).css("border-color","#dee2e6");
        if($("#content"+id).val().length === 0){
            $("#content"+id).css("border-color","red");
        }else{
            data.append("content", $("#content" + id).val());
            data.append("id_pregunta", id);
            data.append("_token", $("input[name='_token']").val());
            $.ajax({
                url: "{{route('responder')}}",
                method: "POST",
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",
                success: function(data){
                    $("div[data-id="+id+"]").html('<div class="media col-md-8 col-sm-10 col-12"><div class="media-body"><h5>Respuesta de: '+data.username+
                                                            '</h5><p>'+data.Contenido+'</p><span style="color: gray">'+
                                                            data.created_at+'</span></div></div>');
                }
            });
        }
    }

    function eliminar(id){
        $.ajax({
            url: "{{url('Producto')}}" + "/" + id,
            type: "DELETE",
            data: {"_token": $("input[name='_token']").val()},
            success: function(data){
                window.location.href = "{{route('catalog')}}";
            },
            error: function(error){
                console.log(error);
            }
        });
    }
</script> 
@endif @endauth

@endsection
