<!DOCTYPE html>
<html lang="en">
<head>
  <title>Power Industrial</title>
  <link rel="icon" href="images/logo/LogoPW_310x181_transp.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css" >
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://kit.fontawesome.com/95e544bed9.js" crossorigin="anonymous"></script>
  <style>
  .fakeimg {
    height: 200px;
    background: #aaa;
  }
  .custom-dropdown{
      display:block;
      width:100%;
      padding:.25rem 1.5rem;
      clear:both;
      font-weight:400;
      color:#212529;
      text-align:inherit;
      white-space:nowrap;
      background-color:transparent;
      border:0}
      .custom-dropdown:focus,.custom-dropdown:hover
      {color:#16181b;
      text-decoration:none;
      background-color:rgb(242, 242, 242, 0.5)}

    .oval{
      
      width: 100%;
      height: 100%;
      -moz-border-radius: 50%;
      -webkit-border-radius: 50%;
      border-radius: 50%;
      background: #ffffff;
      
    }
    
    .opacity{
      background-color:rgb(0, 74, 131);
      opacity: 0.1;
    }

    .border-text{
      text-shadow: 2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff,
                  -1px -1px 0 #fff, 1px -1px #fff, -1px 1px 0 #fff;
    }

    .centrado{
      margin-left: auto;
      margin-right: auto;
    }

.border-icon{
  border-radius: 15% 15% 15% 15%;
  -moz-border-radius: 15% 15% 15% 15%;
  -webkit-border-radius: 15% 15% 15% 15%;
  background-color: white;
  box-shadow: 0 0 80px 6px rgba(255, 255, 255, 0.50);
   } 
.box-shadow{
  box-shadow: 0 0 80px 6px rgba(255, 255, 255, 0.75);
}
  </style>
  @yield('css')
</head>
<body>

<div class="pt-4 py-4" style="margin-bottom:0; background-image: url('/images/img.jpg')">
  <div class="d-sm-flex justify-content-between">
    <div class="ml-4">
      <div class="d-flex">
      <img src="images/logo/LogoPW_310x181_transp.png" alt="Power Industrial logo" style="height: 48px; width: auto">
      <h1 style="font-family: Impact;" class="border-text ml-1">Power Industrial</h1></div>
    </div>
    <div class="d-flex justify-content-end pr-2">
      <div>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Buscar producto" id="txtBuscar" onkeypress="enterSearch(event)">
        <button class="btn btn-light border" onclick="buscarProducto()" type="button"><i class="fas fa-search"></i></button>
      </div>
      </div>
    </div>
  </div>
  
</div>

<nav class="navbar navbar-expand-sm sticky-top navbar-dark justify-content-between" style="background-color: rgb(0, 74, 131); padding-top: 1px; padding-bottom: 1px;">
<a class="navbar-brand" href="{{route('main')}}"><i class="fas fa-home"></i> Inicio</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
    <!--p style="color: white;">&plus;</p-->
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav mr-auto">
      <?php /*<li class="nav-item disabled">
        <a class="nav-link disabled" href="#">Nosotros</a>
      </li>*/ ?>
      <li class="nav-item">
        <a class="nav-link " href="{{ route('catalog')}}">Cat&aacute;logo</a>
      </li> 
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Categor&iacute;a</a>
        <div class="dropdown-menu" style="background-color: rgb(0, 74, 131);">
          <a class="custom-dropdown" style="color: white;" href="{{url('CatalogoLike=PLC')}}">PLC</a>
          <!--div class="dropdown-divider"></div-->
          <a class="custom-dropdown" style="color: white;" href="{{url('CatalogoLike=HMI')}}">HMI</a>
          <a class="custom-dropdown" style="color: white;" href="{{url('CatalogoLike=variador')}}">Variadores</a>
          <a class="custom-dropdown" style="color: white;" href="{{url('CatalogoLike=fuente')}}">Fuentes</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Marcas</a>
        <div class="dropdown-menu" style="background-color: rgb(0, 74, 131);">
          <a class="custom-dropdown" style="color: white;" href="{{url('CatalogoMarca=Allen-Bradley')}}">Allen-Bradley</a>
          <a class="custom-dropdown" style="color: white;" href="{{url('CatalogoMarca=Baldor')}}">Baldor</a>
          <a class="custom-dropdown" style="color: white;" href="{{url('CatalogoMarca=Siemens')}}">Siemens</a>
        </div>
      </li>
      @auth
        @if(auth()->user()->tipo == 'admin')
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Administraci&oacute;n</a>
          <div class="dropdown-menu" style="background-color: rgb(0, 74, 131);">
            <a class="custom-dropdown" href="{{route('producto.alta')}}" style="color: white;">Registrar Producto</a>
            <a class="custom-dropdown" href="{{route('gestionUsuario')}}" style="color: white;">Empleados</a>
          </div>
        </li>
        @endif
      @endauth
    </ul>
    <ul class="navbar-nav">
      @auth
      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{auth()->user()->name . " ". auth()->user()->apellidos}}</a>
      <div class="dropdown-menu dropdown-menu-right" style="background-color: rgb(0, 74, 131)">
        @if (auth()->user()->tipo == "cliente")
        <a href="{{route('Carrito')}}" class="custom-dropdown" style="color: white;"><i class="fas fa-shopping-cart"></i> Carrito</a>
        @endif
        <a href="{{route('logout')}}" class="custom-dropdown" style="color: white;"><i class="fas fa-sign-out-alt"></i> Cerrar sesi&oacute;n</a>
      </div>
      </li>
      @endauth
      @guest
      <li class="nav-item">
        <a href="#" class="nav-link" data-toggle="modal" data-target="#myModal"><i class="fas fa-user-circle"></i> Iniciar sesi&oacute;n</a>
      </li>
      @endguest
    </ul>
  </div>  
</nav>

@yield('contenido')

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
        <!-- Modal Header 
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>-->
  
        <!-- Modal body -->
        <div class="modal-body">
          <div>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div style="margin-top: 100px;">
            <h4 style="text-align: center;">Iniciar sesi&oacute;n</h4>
            <img src="/images/logo/LogoPW_800x400_letras_transp.png" alt="Logo Power Industrial" class="img-fluid rounded">
            <form method="POST" action="{{route('login')}}">
              @csrf
              <div class="form-group">
                <label for="email">Correo:</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{old('email')}}">
                @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
              <div class="form-group">
                <label for="password">Contrase&ntilde;a</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
              <div class="form-group">
                <div class="d-flex justify-content-center">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            Mantener sesi&oacute;n activa
                        </label>
                    </div>
                </div>
              </div>
              <div class="d-flex justify-content">
                <div class="mr-auto"></div>
                <div>
                  <button class="btn btn-light border" style="width: 100%">ENTRAR</button>
                </div>
                <div class="ml-auto"></div>
              </div>
            </form>
            <div class="d-flex justify-content">
              <div class="mr-auto"></div>
              <div class="">
                <?php /* @if (Route::has('password.request'))
                  <a href="{{route('password.request')}}" style="text-align: center; color: black; font-weight: 500">
                    <u>&iquest;Olvidaste la contrase&ntilde;a?</u>
                  </a>
                @endif */ ?>
                <div class="d-flex justify-content">
                  <div class="mr-auto"></div>
                  <div>
                    <a href="{{route('register')}}" style="text-align: center">Reg&iacute;strate</a>
                  </div>
                  <div class="ml-auto"></div>
                </div>
              </div>
              <div class="ml-auto"></div>
            </div>
          </div>
        </div>
  
        <!-- Modal footer 
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>-->
  
      </div>
    </div>
  </div>
  </body>
  <footer>
    <div class="container-fluid pt-4" style="padding-bottom: 25px; background-color: rgb(0, 74, 131)">
      <div class="row d-flex justify-content-center">
          <div class="col-md-2 col-6 text-center pb-2">
            <a href="https://www.facebook.com/Power-industrial-1916827805209274/">
              <i class="fab fa-facebook-square icon-2x, border-icon" style="color: rgb(51, 51, 255); font-size: 3em;"></i>
            
            <p style="font-size: 0.95em; color:white"><br> Facebook<br>Power Industrial</p></a>
          </div>
          <div class="col-md-2 col-6 text-center">
            <i class="fab fa-whatsapp " style="color: green; font-size: 3em; background-color: white; 
            border-radius: 50% 50% 50% 50%; box-shadow: 0 0 80px 6px rgba(255, 255, 255, 0.50);"></i>
            <p style="font-size: 0.95em; color:white"> <br>Whatsapp<br>Cel. 3311 07 86 69</p>
          </div>
          <div class="col-md-2 col-6 text-center">
            <div class="d-flex justify-content-center pb-3"><div>
              <a href="https://www.mercadolibre.com.mx/perfil/POWER+INDUSTRIAL">
                <img src="/images/ML icon2.png" style="width: 4em; height; 
                auto; box-shadow: 0 0 80px 6px rgba(255, 255, 255, 0.50); background-color: white;
                border-radius: 60% 60% 60% 60%;">
                <p style="font-size: 0.95em; color:white;"><br>Mercado Libre</p>
              </a>
            </div></div>
          </div>
          <div class="col-md-auto col-6 text-center">
            <i class="far fa-envelope" style="font-size: 3em;color: red; background-color: white; 
            box-shadow: 0 0 80px 6px rgba(255, 255, 255, 0.50); border-radius: 80% / 20%;"></i>
            <p style="font-size: 0.95em; color:white"><br>Correos:<br>power.industrial1@gmail.com<br>
            renato_muro@yahoo.com.mx</p>
          </div>
      </div>
      <div class="text-center pt-4">
        <p style="color: white;"><br><br>Power Industrial &copy; 2020</p>
      </div>
    </div>
  </footer>
  <script>
    function buscarProducto(){
      if($('#txtBuscar').val().length > 0){
        window.location.href = "{{url('CatalogoLike=')}}" + $('#txtBuscar').val();
      }
    }

    function enterSearch(evt){
      if(evt.which == 13 || evt.keyCode == 13){
        buscarProducto();
      }
    }
  </script>
  @yield('Scripts')
  </html>
  