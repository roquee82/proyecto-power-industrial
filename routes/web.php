<?php

//use App;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
;
Route::get('/', 'ProductController@index')->name('main');

Route::get("/Catalogo", function(){
    //$datos = App\Producto::all();
    $datos = App\Producto::where('Estado','activo')->orWhere('Estado','like','oferta')
                ->orderByRaw('Fecha_publicacion DESC')->get();
    $count = 0;
    if(auth()->check()){
        foreach($datos as $producto){
            if(App\Carrito::where([['usuario','=',auth()->user()->email],['id_producto','=',$producto->id]])
                ->count() != 0){
                $datos[$count]["carrito"] = true;
            }else{
                $datos[$count]["carrito"] = false;
            }
            $count++;
        }
    }
    
    return view('Catalogo', compact('datos'));
})->name("catalog");

Route::get("/CatalogoSort={filter}", 'ProductController@filtrar');
Route::get("/CatalogoMarca={search}", 'ProductController@buscarMarca');
Route::get("/CatalogoLike={search}", 'ProductController@buscar');


Route::get("/Producto/{id}", 'ProductController@ver');
Route::delete("/Producto/{id}", 'ProductController@borrarProducto');
Route::get("editarProducto/{id}", 'ProductController@formularioEdicion');
Route::put("editarProducto/{id}", 'ProductController@editar');
Route::post('/Producto', 'ControlPreguntas@agregar')->name('commit');
//------------------------------C Panel-----------------------------------------------

Route::get('/cpanel','Web\CpanelController@home')->name("cpanel");
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/test', function(){
    return view('welcome');
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::post('/Responder', 'ControlPreguntas@responder')->name('responder');
//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/AltaProducto', 'ProductController@formularioRegistro')->middleware('auth')->name('producto.alta');
Route::post('AltaProducto', 'ProductController@registrar');

Route::get('/Carrito', 'ControlCarrito@listar')->middleware('auth')->name('Carrito');
Route::post('/agregarCarrito', 'ControlCarrito@agregar');
Route::delete('/quitarCarrito/{id}', 'ControlCarrito@quitar');

Route::get('/gestionarUsuarios', 'UserControl@gestionUsuario')->middleware('auth')->name('gestionUsuario');
Route::post('/agregarEmpleado', 'UserControl@registrarEmpleado')->name('empleado.agregar');
Route::delete('/bajaEmpleado/{id}', 'UserControl@bajaEmpleado');