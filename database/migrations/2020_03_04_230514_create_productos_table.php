<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("Nombre");
            $table->decimal("Precio", 8, 2);
            $table->string("Marca");
            $table->text("Descripcion");
            $table->integer("Vistas");
            $table->timestamp("Fecha_publicacion");
            $table->timestamp("Fecha_eliminacion")->nullable();
            $table->unsignedBigInteger("publicante");
            $table->foreign("publicante")->references("id")->on('users')->onUpdate('cascade');
            $table->string("Estado");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
