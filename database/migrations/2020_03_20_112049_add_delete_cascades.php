<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeleteCascades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('respuestas', function (Blueprint $table) {
            $table->dropForeign(['ID_Pregunta']);
        });
        Schema::table('preguntas', function (Blueprint $table) {
            $table->dropForeign(['ID_Producto']);
        });

        Schema::table('respuestas', function (Blueprint $table) {
            $table->foreign('ID_Pregunta')->references('id')->on('preguntas')
                ->onDelete("cascade")->change();
        });
        Schema::table('preguntas', function (Blueprint $table) {
            $table->foreign('ID_Producto')->references('id')->on('productos')
                ->onDelete("cascade")->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respuestas', function (Blueprint $table) {
            $table->dropForeign(['ID_Pregunta']);
        });
        Schema::table('preguntas', function (Blueprint $table) {
            $table->dropForeign(['ID_Producto']);
        });
        Schema::table('respuestas', function (Blueprint $table) {
            $table->foreign('ID_Pregunta')->references('id')->on('preguntas')
                ->onDelete("restrict")->change();
        });
        Schema::table('preguntas', function (Blueprint $table) {
            $table->foreign('ID_Producto')->references('id')->on('productos')
                ->onDelete("restrict")->change();
        });
        
    }
}
