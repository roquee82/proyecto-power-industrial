<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Carrito extends Model
{
    public $incrementing = 'false';
    protected $primaryKey = 'usuario';
    protected $keyTipe = 'string';

    protected $fillable = [
        'usuario', 'id_producto'
    ];

    public function productos(){
        return $this->hasMany(Producto::class);
    }
    
    public function usuario(){
        return $this->belongsTo(User::class);
    }
}
