<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    public function usuario(){
        return $this->belongsTo(User::class);
    }

    protected $fillable = [
        'Nombre', 'Precio','Marca','Descripcion','Vistas','Fecha_publicacion','publicante','Estado','image', 'linkMercado'
    ];

    protected $casts = [
        'Fecha_publicacion' => 'datetime'
    ];
}
