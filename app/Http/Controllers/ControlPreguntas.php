<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Pregunta;
use App\Respuesta;

class ControlPreguntas extends Controller
{
    public function agregar(Request $request){
        $pregunta = new Pregunta;
        $pregunta->username = $request->username;
        $pregunta->Contenido = $request->description;
        $pregunta->ID_Producto = $request->prod_id;

        $pregunta->save();

        return back()->with('confirm', 'Pregunta publicada');
    }

    public function responder(Request $request){
        $respuesta_pregunta = new Respuesta;
        $response = [];
        if(request()->ajax()){
            if(auth()->check() && auth()->user()->tipo == "admin"){
                $respuesta_pregunta->username = auth()->user()->name . ' ' . auth()->user()->apellidos;
                $respuesta_pregunta->Contenido = $request->content;
                $respuesta_pregunta->ID_pregunta = $request->id_pregunta;
                $respuesta_pregunta->save();
                $response = $respuesta_pregunta;
            }
        }
        return $response;
    }
}
