<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserControl extends Controller
{
    public function gestionUsuario(){
        if(auth()->user()->tipo == 'admin' || auth()->user()->tipo == 'empleado'){
            $employees = User::where('id','<>', auth()->id())->where(function($query){
                $query->where('tipo', 'admin')->orWhere('tipo', 'empleado');
            })->get();
            return view('Usuarios', compact('employees'));
        }else{
            return redirect('/');
        }
    }

    public function registrarEmpleado(){
        $data = request();
        if(auth()->check()){
            if(auth()->user()->tipo == 'admin'){
                return User::create([
                    'name' => $data->name,
                    'apellidos' => $data->apellidos,
                    'email' => $data->email,
                    'password' => Hash::make($data->password),
                    'tipo' => $data->tipo,
                    'telefono' => $data->phone
                ]);
            }
        }
        return 'failure';
    }

    public function bajaEmpleado($id){
        if(auth()->check()){
            if(auth()->user()->tipo == 'admin'){
                $user = User::find($id);
                $user->tipo = "cliente";
                $user->save();
                return "success";
            }
        }
        return 'failure';
    }
}
