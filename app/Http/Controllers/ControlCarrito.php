<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Carrito;
use Illuminate\Support\Facades\DB;

class ControlCarrito extends Controller
{
    public function agregar(){
        $request = request();
        if($request->ajax() && auth()->check()){
            Carrito::create([
                'usuario' => auth()->user()->email,
                'id_producto' => $request->prod_id]);
            return json_encode(['success' => true]);
        }
    }

    public function verCarrito(){
        if(auth()->check()){

        }
        return back();
    }

    public function listar(){
        if(auth()->check()){
            $datos = DB::table('productos')->join('carritos', 'carritos.id_producto', "=", 'productos.id')->
                where('usuario', auth()->user()->email)->orderByRaw('carritos.created_at DESC')->
                select('productos.*')->get();
            return view('Carrito',compact('datos'));
        }
    }

    public function quitar($id){
        $success = 'failure';
        if(auth()->check()){
            DB::table('carritos')->where([['usuario', '=', auth()->user()->email],['id_producto', '=', $id]])->delete();
            $success = 'success';
        }
        return $success;
    }
}
