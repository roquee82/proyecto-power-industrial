<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use File;
use Request;
use Illuminate\Support\Facades\DB;
use App\Pregunta;
use App\Producto;
use App\Carrito;

class ProductController extends Controller
{
    public function filtrar($filter){
        $products = [];
        $products['permission'] = false;
        $count = 0;

        if(Request::ajax()){
            $datos = Producto::select('Nombre','Marca','Precio','id','image')->where('Estado','activo')
                            ->orWhere('Estado','like','oferta')->orderByRaw($filter.'')->get();
            if(auth()->check()){
                if(auth()->user()->tipo == 'admin'){
                    $products['permission'] = 'admin';
                }else{
                    $products['permission'] = 'client';
                    foreach($datos as $producto){
                        if(Carrito::where([['usuario','=',auth()->user()->email],['id_producto','=',$producto->id]])
                            ->count() != 0){
                            $datos[$count]["carrito"] = true;
                        }else{
                            $datos[$count]["carrito"] = false;
                        }
                        $count++;
                    }
                }
            }
            $products['products'] = $datos;
        }else{
            $products = back();
        }
        return $products;
    }

    public function ver($id){
        $info = DB::table('productos')->where([
            ['id','=', $id],
            ['Estado', '=', 'activo']
        ])->orWhere([['id', '=', $id],['Estado','like','oferta']])->get();
        if(count($info) < 1){
            return redirect(route('catalog'));
        }
        $preguntas = Pregunta::where('ID_Producto', $id)->orderByRaw('created_at DESC')->get();
        $count = 0;
        foreach ($preguntas as $key) {
            $preguntas[$count]["respuesta"] = DB::table('respuestas')->where("ID_Pregunta", $key->id)->first();
            $count++;
        }
        return view('verProducto', compact('info', 'preguntas'));
    }

    public function buscar($search){
        $datos = Producto::where('Nombre','like','%'.$search.'%')->latest()->get();
        $count = 0;
        if(auth()->check()){
            foreach($datos as $producto){
                if(Carrito::where([['usuario','=',auth()->user()->email],['id_producto','=',$producto->id]])
                    ->count() != 0){
                    $datos[$count]["carrito"] = true;
                }else{
                    $datos[$count]["carrito"] = false;
                }
                $count++;
            }
        }
        return view('Catalogo', compact('datos'));
    }

    public function formularioRegistro(){
        if(auth()->user()->tipo === 'admin'){
            return view('formularioProducto');
        }else{
            return redirect("/");
        }
    }
    
    public function buscarMarca($search){
        $datos = Producto::where("Marca", 'like', '%'.$search.'%')->latest()->get();
        $count = 0;
        if(auth()->check()){
            foreach($datos as $producto){
                if(Carrito::where([['usuario','=',auth()->user()->email],['id_producto','=',$producto->id]])
                    ->count() != 0){
                    $datos[$count]["carrito"] = true;
                }else{
                    $datos[$count]["carrito"] = false;
                }
                $count++;
            }
        }
        return view('Catalogo', compact('datos'));
    }

    public function registrar(){
        $request = request();
        $this->validate($request,[
            'prod_name' => 'required|max:255|string',
            'price' => 'required|numeric|max:100000000',
            'brand' => "nullable|max:255|string",
            'image' => 'nullable|max:4000|image|mimes:png,jpg,jpeg',
            'desc' => 'required|string',
            'user_id' => 'required|numeric',
            'link' => 'nullable|string|max:255'
        ]);

        
        
        if(is_null($request->brand) || $request->unknown_brand){
            $productoMarca = 'Desconocida';
        }else{
            $productoMarca = $request->brand;
        }

        $image = $request->image;
        $productoImage = null;
        if(!is_null($image)){
            $image_name = time() . '-' . rand() . '.' . $image->extension();
            $image->move(public_path('images/productos'), $image_name);
            $productoImage = $image_name;
            /*if(strlen($image_name) > 255){
                $response = back()->withErrors(['image' => 'El nombre del archivo es demasiado grande, cambiar nombre antes de subir']);
            }
            else{
                $response = back()->with('success', 'Imagen subida con éxito')->with('path', $image_name);
            }*/
        }

        $producto = Producto::create([
            'Nombre' => $request->prod_name,
            'Precio' => $request->price,
            'Marca' => $productoMarca,
            'Descripcion' => $request->desc,
            'Vistas' => 0,
            'Fecha_publicacion' => time(),
            'publicante' => $request->user_id,
            'Estado' => $request->status,
            'image' => $productoImage,
            'linkMercado' => $request->link
        ]);

        $id = $producto->id;

        return redirect(url("Producto")."/" . $id);

    }

    public function borrarProducto($id){
        $producto = Producto::find($id);
        if($producto == null){
            return json_encode(["respuesta" => "failure"]);
        }
        if(!is_null($producto->image)){
            $img_link = 'images/productos/'.$producto->image;
            if(File::exists($img_link)){
                File::delete($img_link);
            }
        }
        $producto->delete();
        return json_encode(["respuesta" => "success"]);
    }

    public function formularioEdicion($id){
        $datos = Producto::find($id);
        if(is_null($datos)){
            return redirect(route('catalog'));
        }
        return view("editarProducto", compact('datos'));
    }

    public function editar($id){
        $request = request();
        $this->validate($request,[
            'prod_name' => 'required|max:255|string',
            'price' => 'required|numeric|max:100000000',
            'brand' => "nullable|max:255|string",
            'image' => 'nullable|max:4000|image|mimes:png,jpg,jpeg',
            'desc' => 'required|string',
            'link' => 'nullable|string|max:255'
        ]);
        $producto = Producto::find($id);
        $producto->Nombre = $request->prod_name;
        $producto->Precio = $request->price;
        $producto->Descripcion = $request->desc;
        $producto->Estado = $request->status;
        $producto->linkMercado = $request->link;
        if(is_null($request->brand) || $request->unknown_brand){
            $producto->Marca = 'Desconocida';
        }else{
            $producto->Marca = $request->brand;
        }
        if($request->imgflag){
            if(!is_null($producto->image)){
                $img_link = 'images/productos/'.$producto->image;
                if(File::exists($img_link)){
                    File::delete($img_link);
                }
                $producto->image = null;
            }
            $image = $request->image;
            if(!is_null($image)){
                $image_name = time() . '-' . rand() . '.' . $image->extension();
                $image->move(public_path('images/productos'), $image_name);
                $producto->image = $image_name;
            }
        }
        $producto->save();

        return redirect(url("Producto") . "/" . $id);
    }

    public function index(){
        $products = Producto::where('Estado','oferta')->get();
        $products2 = [];
        $datos = [];
        $count = 0;
        if(count($products) < 6){
            $products2 = Producto::where('Estado','activo')->latest()->take((6 - count($products)))->get();
        }
        foreach($products as $prod){
            $datos[$count] = $prod;
            $count++;
        }
        foreach($products2 as $prod){
            $datos[$count] = $prod;
            $count++;
        }
        return view('mainPage', ['productos' => $datos]);
    }
}
